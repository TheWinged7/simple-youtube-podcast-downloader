## YouTube-dlp to MP3 playlist with sponsorblodk

a simple setup to download YouTube playlists from a `.txt` file, and save as mp3s

# Table of Contents
1. [Prerequisites](#Prerequisites)
2. [Setup](#Setup)
    1. [Edit config](#Edit-config)
    2. [Create cron job](#Create-cron-job)
1. [playlists.txt](#playlists.txt)

### Prerequisites
1. yt-dlp and FFMPEG with FFProbe installed. This can be either a local install or a docker container.
    - The included script uses the container `thr3a/yt-dlp:`

### Setup
#### Edit config 

1. Create a `playlists.txt` file to create a queue of youtube playlists to download. 
2. If using local installs of yt-dlp and FFMPEG some edits need to be made to the `autoDownload.sh` script
    1. uncomment `--ffmpeg-location` in the `yt-dlp.conf` file and add the correct path to FFMPEG.
    2. uncomment the windows version of the download command, and update the path to your `yt-dlp` path
    3. comment out the Dowcker version of the download command
3. Add your path to  `autoDownload.sh` script

#### Create cron job
Add the below to your cron file. The reccomended method is using `crontab -e`
```bash
20 17 * * * <path/to/autoDownload.sh>
``` 

#### playlists.txt

```text
<URL> <Filter>
```

e.g. 
```yaml
# download a playlist starting from episode 2 to epsiode 10, skipping odd episodes
https://www.youtube.com/playlist?list=PL1..... 2:10:2

# download a playlist starting from episode 5 onwards
https://www.youtube.com/playlist?list=PL1..... 5:
```
