#!/usr/bin/env bash

# Load playlist file to variable
path="<Path to where config exist and files will download>"
playlist_file=$(<$path/playlists.txt)

echo "will download new episodes to $path"
ls $path

# Split into array of playlists
readarray -t playlist_array <<<"$playlist_file"

# itterate through playlists 
for playlist_def in "${playlist_array[@]}"
do
    parts=($playlist_def)
    url=${parts[0]}
    filter=${parts[1]}

    # Run command to download
    # Windows version
#    ./bin/yt-dlp.exe -I $filter --config-location ./yt-dlp.conf $url
    # Docker version
    docker run \
        -v $path/:/app: \
        -w /app \
        --rm \
        --name podcast-downloader
        thr3a/yt-dlp \
        --config-location \
        ./yt-dlp.conf \
        -I $filter \
        $url
done